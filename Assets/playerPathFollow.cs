﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerPathFollow : MonoBehaviour
{
    public GameObject target;
    public GameObject exitPoint;

    public GameObject playerStation;

    public bool moving;
    public float moveSpeed;

    public GameObject head;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(target && moving)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * moveSpeed);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, target.transform.rotation, Time.deltaTime * moveSpeed * 10);

            if(Vector3.Distance(transform.position, target.transform.position)< 0.01f)
            {
                if (target.GetComponent<pathPoint>().nextPoint)
                {
                    target = target.GetComponent<pathPoint>().nextPoint;
                }

                if (target.GetComponent<pathPoint>().playerStation)
                {
                    GetComponent<PlayerAnimControl>().SitDown();
                    playerStation = target;
                    //moving = false;

                    //Game
                }

                if(transform.rotation == playerStation.transform.rotation)
                {
                    moving = false;
                    GameStatesControl.instance.GoSitState();
                    target = null;
                }
                
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            GoExit();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "playerStation")
        {
            transform.position = other.transform.position;
            transform.rotation = other.transform.rotation;
            moving = false;
        }
    }

    public void GoExit()
    {
        moving = true;
        target = exitPoint;
        GetComponent<PlayerAnimControl>().walk();
    }
}
