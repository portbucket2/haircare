﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatesControl : MonoBehaviour
{
    public static GameStatesControl instance;

    public GameObject currentChar;
    public HairDryerFlowRandomizer HairDrierFlow;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        GoIdleState();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoIdleState()
    {
        GameManagement.instance.GoToState(0);
    }
    public void GoWalkState()
    {
        GameManagement.instance.GoToState(1);
        currentChar.GetComponent<playerPathFollow>().moving = true;
    }
    public void GoSitState()
    {
        GameManagement.instance.GoToState(2);
        Invoke("GoShampooState", 2f);
    }
    public void GoShampooState()
    {
        GameManagement.instance.GoToState(3);

        OverheadTools.instance.GetTool(0);
    }
    public void GoRinseState()
    {
        GameManagement.instance.GoToState(4);
        OverheadTools.instance.GetTool(1);

        Invoke("FoamTheHairInStates",1f);
    }
    public void GoWashState()
    {
        GameManagement.instance.GoToState(5);
        OverheadTools.instance.GetTool(2);
    }
    public void GoDryState()
    {
        GameManagement.instance.GoToState(6);
        OverheadTools.instance.GetTool(3);
    }

    public void GoCutState()
    {
        GameManagement.instance.GoToState(7);
        OverheadTools.instance.GetTool(4);
    }
    public void GoDoneState()
    {
        GameManagement.instance.GoToState(8);
        currentChar.GetComponent<PlayerAnimControl>().CheerUp();
        Invoke("CharMoveAway", 2f);
        //CharMoveAway();
    }

    public void FoamTheHairInStates()
    {
        currentChar.GetComponent<playerPathFollow>().head.GetComponent<HairDryerFlowRandomizer>().FoamTheHair();
        HairDrierFlow = currentChar.GetComponent<playerPathFollow>().head.GetComponent<HairDryerFlowRandomizer>();
    }

    public void CharMoveAway()
    {
        currentChar.GetComponent<playerPathFollow>().GoExit();
    }
}
