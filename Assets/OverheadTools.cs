﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverheadTools : MonoBehaviour
{
    public GameObject[] tools;
    public static OverheadTools instance;

    public enum Tools
    {
        Shampoo , Hand , Shower , Drier , Scissor , Empty
    }
    public Tools currTool;

    public Vector3 positionMaxLeft;
    public Vector3 positionMaxRight;

    public Vector3 rotationMaxLeft;
    public Vector3 rotationMaxRight;

    public GameObject currentToolObj;
    public GameObject AllToolsHolder;

    public ParticleSystem shampooParticle;
    public ParticleSystem ShowerParticle;

    public Vector3 downPos;
    public Vector3 upPos;

    public bool down;
    public bool up;

    public GameObject scissorSide;
    public GameObject scissorBlade;
    public bool ScissorRun;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //GetTool(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentToolObj)
        {
            MoveCurrentTool(InputSystem.instance.progressionOutput);
        }

        if (down)
        {
            for (int i = 0; i < tools.Length; i++)
            {
                if(tools[i] == currentToolObj)
                {
                    tools[i].transform.localPosition = Vector3.Lerp(tools[i].transform.localPosition, downPos, Time.deltaTime * 5);
                    if (Vector3.Distance(tools[i].transform.localPosition, downPos) < 0.1f)
                    {
                        down = false;
                    }
                }
                else
                {
                    tools[i].transform.localPosition = Vector3.Lerp(tools[i].transform.localPosition, upPos, Time.deltaTime * 5);
                }
                
            }
            
            if(Vector3.Distance(transform.position, downPos) < 0.1f)
            {
                down = false;
            }
        }

        if(currTool == Tools.Scissor)
        {
            if (ScissorRun)
            {
                
                scissorSide.transform.localPosition = Vector3.MoveTowards(scissorSide.transform.localPosition, new Vector3(10, 0, 0), Time.deltaTime * 10);
                if(scissorSide.transform.localPosition ==new Vector3(10, 0, 0))
                {
                    ScissorRun = false;

                    GetTool(5);

                    GameStatesControl.instance.GoDoneState();
                }
            }
        }
        //else if(up)
        //{
        //    transform.position = Vector3.Lerp(transform.position, upPos, Time.deltaTime * 5);
        //    if (Vector3.Distance(transform.position, upPos) < 0.5f)
        //    {
        //        up = false;
        //    }
        //}
        //
    }

    public void GetTool(int tool)
    {
        for (int i = 0; i < tools.Length; i++)
        {
            if(i == tool)
            {
                tools[i].SetActive(true);
            }
            else
            {
                //tools[i].SetActive(false);
            }
        }

        currTool = (Tools)tool;
        currentToolObj = tools[tool];

        down = true;
    }

    public void MoveCurrentTool(float inputSwipe)
    {
        float per = (inputSwipe + 1) / 2;
        //currentToolObj.transform.localPosition = Vector3.Lerp(positionMaxLeft, positionMaxRight, per);
        AllToolsHolder.transform.localPosition = Vector3.Lerp(positionMaxLeft, positionMaxRight, per);

        if (currTool == Tools.Shampoo)
        {
            if (Input.GetMouseButtonDown(0))
            {
                shampooParticle.Play();
            }
            if (!Input.GetMouseButton(0))
            {
                shampooParticle.Stop();
            }
        }
        else
        {
            shampooParticle.Stop();
        }

        if (currTool == Tools.Hand)
        {
            if (Input.GetMouseButton(0))
            {
                currentToolObj.GetComponentInChildren<Animator>().speed = 1;
            }
            else
            {
                currentToolObj.GetComponentInChildren<Animator>().speed = 0;
            }
            
        }

        if (currTool == Tools.Shower)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ShowerParticle.Play();
                shampooParticle.Clear();

            }
            if (Input.GetMouseButton(0))
            {
                
                GameStatesControl.instance.HairDrierFlow.WashHair(per);
            }
            else
            {
                ShowerParticle.Stop();
            }
        }
        else
        {
            ShowerParticle.Stop();
        }

        if (currTool == Tools.Drier)
        {
            if (Input.GetMouseButtonDown(0))
            {

                GameStatesControl.instance.HairDrierFlow.EnableDrierMode();
            }
            if (Input.GetMouseButton(0))
            {

                GameStatesControl.instance.HairDrierFlow.HairDrierRunning(per);
            }
            if (Input.GetMouseButtonUp(0))
            {
                GameStatesControl.instance.HairDrierFlow.EnableStraight();
            }
        }
        else
        {
            //GameStatesControl.instance.HairDrierFlow.EnableStraight();
        }
        if (currTool == Tools.Scissor)
        {
            GameStatesControl.instance.HairDrierFlow.EnableStraight();
        }
    }

    public void CloseAllTools()
    {
        for (int i = 0; i < tools.Length; i++)
        {
            tools[i].SetActive(false);
        }
    }

    public void ScissorCut()
    {
        ScissorRun = true;
        scissorSide.GetComponentInChildren<Animator>().enabled = false;

        GameStatesControl.instance.HairDrierFlow.cuttingPlane.CUTTT(scissorBlade);
    }

    


}
