﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimControl : MonoBehaviour
{
    public Animator playerAnim;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SitDown()
    {
        playerAnim.SetBool("sit", true);
    }

    public void CheerUp()
    {
        playerAnim.SetTrigger("victory");
        playerAnim.SetBool("sit", false);
    }
    public void walk()
    {
        playerAnim.SetTrigger("walk");
    }
}
