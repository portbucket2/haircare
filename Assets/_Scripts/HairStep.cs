﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairStep : MonoBehaviour
{
    public ParticleSystem foamParticle;
    public ParticleSystem foamParticleWashing;

    public bool washed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (foamParticle)
            {
                foamParticle.Play();
            }
            
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            if (foamParticle)
            {
                foamParticle.Stop();
                foamParticleWashing.Play();
            }

        }
    }

    public void WashPaticle()
    {
        if (!washed)
        {
            if (foamParticle )
            {
                foamParticle.Stop();
                foamParticleWashing.Play();

                
            }
            washed = true;
            GetComponentInParent<HairDryerFlowRandomizer>().washedIndex += 1;
        }
        
        
    }

    public void FoamParticle()
    {
        if (foamParticle)
        {
            foamParticle.Play();
        }
    }
}
