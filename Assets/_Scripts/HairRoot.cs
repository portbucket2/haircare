﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairRoot : MonoBehaviour
{
    public HairStep[] hairSteps;

    public float sinAngle;
    public float sinAngleFR;

    public float[] sinMultiplier;
    public float[] sinMultiplierFR;

    public float frequencyApp;
    public float frequency;
    public float Amplitude;

    public Quaternion[] initialRots;

    public GameObject[] initialRotRefsTangled;
    public GameObject[] initialRotRefsStraight;
    public GameObject[] initialRotRefsDrying;

    public float offsetAngle;

    public GameObject follower;
    public GameObject followTar;
    public GameObject hairRotator;

    public Vector3 gap;

    public float AmpMultiSide;
    public float AmpMultiFrontBack;

    public Vector2 lastFrameGap;

    public bool decreasing;
    public bool decreasingZ;

    public bool leftS;
    public bool rightS;
    public bool frontS;
    public bool backS;

    public int initialRotIndex;

    public bool HairDrierMode;
    public bool HairStraightMode;
    public float HairDrierIntensity;

    public float TangledModeMulti;

    public float swingDisatnceThreshold;

    public GameObject HairMesh;

    //public bool 

    //float offsettedSinMulti;
    // Start is called before the first frame update
    void Start()
    {
        TangleIt();
        //initialRots[0] = hairSteps[0].transform.localRotation;
        //initialRots[1] = hairSteps[1].transform.localRotation;
        //initialRots[2] = hairSteps[2].transform.localRotation;
        //initialRots[3] = hairSteps[3].transform.localRotation;

        sinMultiplier = new float[hairSteps.Length];
        sinMultiplierFR = new float [hairSteps.Length];

        initialRots = new Quaternion[hairSteps.Length];
        

        frequencyApp =  frequency + Random.Range(-30, 30);
        Amplitude += Random.Range(-2, 2);
        TangledMode();

        //sinAngle = Random.Range(0, 100);

        follower = Instantiate(follower, followTar.transform.position, followTar.transform.rotation);

        float Scale = Random.Range(0.9f, 1.1f);
        //transform.localScale = new Vector3(Scale, Scale, Scale);
    }

    // Update is called once per frame
    void Update()
    {
        follower.transform.position = Vector3.Lerp(follower.transform.position, followTar.transform.position, Time.deltaTime*5);
        gap = followTar.transform.InverseTransformPoint(follower.transform.position);
        hairRotator.transform.localRotation = Quaternion.Euler(new Vector3(-gap.z * 5, 0, gap.x *5));

        if((Mathf.Abs( gap.x) < lastFrameGap.x))
        {
            decreasing = true;
        }
        else
        {
            decreasing = false;
            if(gap.x > swingDisatnceThreshold)
            {
                LeftSwing();
            }
            else if (gap.x < -swingDisatnceThreshold)
            {
                RightSwing();
            }
            else
            {
                decreasing = true;
            }

        }
        lastFrameGap.x = Mathf.Abs(gap.x);

        if ((Mathf.Abs(gap.z) < lastFrameGap.y))
        {
            decreasingZ = true;
        }
        else
        {
            decreasingZ = false;
            if (gap.z > swingDisatnceThreshold)
            {
                BackSwing();
            }
            else if (gap.z < -swingDisatnceThreshold)
            {
                FrontSwing();
            }
            else
            {
                decreasingZ = true;
            }

        }
        lastFrameGap.y = Mathf.Abs(gap.z);


        if (Input.GetKey(KeyCode.L))
        {
            LeftSwing();
        }

        if (Input.GetKey(KeyCode.K))
        {
            RightSwing();
        }

        if (Input.GetKey(KeyCode.U))
        {
            TangledMode();
        }
        if (Input.GetKey(KeyCode.I))
        {
            StraightMode();
        }
        if (Input.GetKey(KeyCode.O))
        {
            HairDrierModeFun();
        }


        sinAngle += Time.deltaTime * frequencyApp;
        sinAngleFR += Time.deltaTime * frequencyApp;

        if (!HairDrierMode)
        {
            if (AmpMultiSide > 0)
            {
                AmpMultiSide -= Time.deltaTime * 0.3f;
            }
            else
            {
                AmpMultiSide = 0;
            }

            if (AmpMultiFrontBack > 0)
            {
                AmpMultiFrontBack -= Time.deltaTime * 0.3f;
            }
            else
            {
                AmpMultiFrontBack = 0;
            }
        }
        


        if (sinAngle >= 360)
        {
            sinAngle -= 360;
        }
        if (sinAngleFR >= 360)
        {
            sinAngleFR -= 360;
        }

        for (int i = 0; i < hairSteps.Length; i++)
        {
            GetSinAngleOnAStep(i, sinAngle, sinAngleFR);
        }
        //GetSinAngleOnAStep(0, sinAngle,sinAngleFR);
        //GetSinAngleOnAStep(1, sinAngle,sinAngleFR);
        //GetSinAngleOnAStep(2, sinAngle,sinAngleFR);
        //GetSinAngleOnAStep(3, sinAngle, sinAngleFR);
        //GetSinAngleOnAStep(4, sinAngle, sinAngleFR);

        SwitchInitialRot();
    }

    public void GetSinAngleOnAStep(int i ,float Angle , float AngleFR)
    {
        Angle += (offsetAngle * i);
        if(Angle >= 360)
        {
            Angle -= 360;
        }
        else if (Angle <= 0)
        {
            Angle += 360;
        }

        AngleFR += (offsetAngle * i);
        if (AngleFR >= 360)
        {
            AngleFR -= 360;
        }
        else if (AngleFR <= 0)
        {
            AngleFR += 360;
        }

        sinMultiplier[i] = Mathf.Sin(Mathf.Deg2Rad * (Angle));
        sinMultiplierFR[i] = Mathf.Sin(Mathf.Deg2Rad * (AngleFR));

        Quaternion targetRot = initialRots[i] * Quaternion.Euler(new Vector3(Amplitude * TangledModeMulti * i* AmpMultiFrontBack * sinMultiplierFR[i], Amplitude * TangledModeMulti* AmpMultiSide * sinMultiplier[i], 0));

        hairSteps[i].transform.localRotation = Quaternion.Lerp(hairSteps[i].transform.localRotation,targetRot, Time.deltaTime * 5);
        //Vector3 degrot = Quaternion.ToEulerAngles(hairSteps[i].transform.localRotation) * Mathf.Rad2Deg;

        //hairSteps[i].transform.localRotation = Quaternion.Euler(degrot.x, degrot.y, 0);
    }

    public void LeftSwing()
    {
        AmpMultiSide = 1;
        //sinAngle = 180;

        sinAngle = Mathf.Lerp(sinAngle, 180, Time.deltaTime * 20);

        leftS = true;
        rightS = false;
        //frontS = false;
        //backS = false;
    }
    public void RightSwing()
    {
        AmpMultiSide = 1;
        //sinAngle = 0;

        sinAngle = Mathf.Lerp(sinAngle, 0, Time.deltaTime * 20);

        leftS = false;
        rightS = true;
        //frontS = false;
        //backS = false;
    }
    public void FrontSwing()
    {
        AmpMultiFrontBack = 1;
        //sinAngle = 180;
        //sinAngleFR = 180;

        sinAngleFR = Mathf.Lerp(sinAngleFR, 180, Time.deltaTime * 20);

        //leftS = false;
        //rightS = false;
        frontS = true;
        backS = false;
    }
    public void BackSwing()
    {
        AmpMultiFrontBack = 1;
        //sinAngle = 0;
        //sinAngleFR = 0;

        sinAngleFR = Mathf.Lerp(sinAngleFR, 0, Time.deltaTime *20);

        //leftS = false;
        //rightS = false;
        frontS = false;
        backS = true;
    }
    
    public void SwitchInitialRot()
    {
        //initialRots[  0] = Quaternion.Lerp(initialRots[0], initialRotRefs[(initialRotIndex * 4) + 0].transform.localRotation, Time.deltaTime * 10); 
        //initialRots[ 1] =  Quaternion.Lerp(initialRots[1], initialRotRefs[(initialRotIndex * 4) + 1].transform.localRotation, Time.deltaTime * 10); 
        //initialRots[ 2] =  Quaternion.Lerp(initialRots[2], initialRotRefs[(initialRotIndex * 4) + 2].transform.localRotation, Time.deltaTime * 10); 
        //initialRots[ 3] = Quaternion.Lerp(initialRots[3], initialRotRefs[(initialRotIndex * 4) + 3].transform.localRotation, Time.deltaTime * 10);

        if (HairDrierMode)
        {
            for (int i = 0; i < hairSteps.Length; i++)
            {
                initialRots[i] = Quaternion.Lerp(initialRotRefsStraight[ i].transform.localRotation, initialRotRefsDrying[i].transform.localRotation, HairDrierIntensity);
                
            }
        }
        else
        {
            for (int i = 0; i < hairSteps.Length; i++)
            {
                if(initialRotIndex == 0) 
                {
                    initialRots[i] = Quaternion.Lerp(initialRots[i], initialRotRefsTangled[ i].transform.localRotation, Time.deltaTime * 20);
                }
                else if (initialRotIndex == 1)
                {
                    initialRots[i] = Quaternion.Lerp(initialRots[i], initialRotRefsStraight[i].transform.localRotation, Time.deltaTime * 20);
                }


            }
        }
    }


    public void TangleIt()
    {

        Vector3 RondomRot = new Vector3(Random.Range(-20,50),0, 0);
        initialRotRefsTangled[0].transform.localRotation = Quaternion.Euler(RondomRot);

        RondomRot = new Vector3(Random.Range(-90,-50),0, 0);
        initialRotRefsTangled[1].transform.localRotation = Quaternion.Euler(RondomRot);

        RondomRot = new Vector3(Random.Range(-90, -20), Random.Range(-70, 70), 0);
        initialRotRefsTangled[2].transform.localRotation = Quaternion.Euler(RondomRot);

        RondomRot = new Vector3(Random.Range(-90, -60), Random.Range(-70, 70), 0);
        initialRotRefsTangled[3].transform.localRotation = Quaternion.Euler(RondomRot);

        RondomRot = new Vector3(Random.Range(-90, -60), Random.Range(-70, 70), 0);
        initialRotRefsTangled[4].transform.localRotation = Quaternion.Euler(RondomRot);

    }

    public void TangledMode()
    {
        initialRotIndex = 0;

        FrontSwing();
        frequencyApp = frequency + Random.Range(-30, 30);
        HairDrierMode = false;
        HairStraightMode = false;

        TangledModeMulti = 0.2f;
    }
    public void StraightMode()
    {
        initialRotIndex = 1;

        FrontSwing();
        frequencyApp = frequency + Random.Range(-30, 30);
        HairDrierMode = false;
        HairStraightMode = true;

        TangledModeMulti = 1f;
    }

    public void HairDrierModeFun()
    {
        initialRotIndex = 2;
        FrontSwing();

        frequencyApp = frequency * Random.Range(1.3f, 1.5f);

        HairDrierMode = true;
        HairStraightMode = false;

        TangledModeMulti =1f;
    }
}
