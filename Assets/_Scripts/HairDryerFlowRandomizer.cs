﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HairDryerFlowRandomizer : MonoBehaviour
{
    public HairRoot[] hairRoots;

    public List<HairStep> hairSteps;
    public List<HairStep> hairStepsSorted;

    [Range(0,29)]
    public int pointer;

    public int index;

    bool wash;
    float m;

    public int washedIndex;
    public bool washDone;
    public bool straighened;

    public CuttingPlane cuttingPlane;
    // Start is called before the first frame update
    void Start()
    {
        
        for (int i = 0; i < hairRoots.Length; i++)
        {


            for (int j = 0; j < hairRoots[i].hairSteps.Length; j++)
            {
                hairSteps.Add(hairRoots[i].hairSteps[j]);
            }
        }

        //SortHairSteps();
    }

    // Update is called once per frame
    void Update()
    {
        if (OverheadTools.instance.currTool == OverheadTools.Tools.Drier)
        {
            
        }
        

        if (Input.GetKeyDown(KeyCode.S))
        {
            SortHairSteps();

        }

        if (wash)
        {
            WashSequentially();
            wash = false;
        }
    }

    public void AllStraightMode()
    {
        for (int i = 0; i < hairRoots.Length; i++)
        {
            hairRoots[i].StraightMode();
        }
    }

    public void AllHairDrierMode()
    {
        for (int i = 0; i < hairRoots.Length; i++)
        {
            hairRoots[i].HairDrierModeFun();
        }
    }

    public void AllTangledMode()
    {
        for (int i = 0; i < hairRoots.Length; i++)
        {
            hairRoots[i].TangledMode();
        }
    }

    public void SortHairSteps()
    {
        for (int i = 0; i < hairSteps.Count; i++)
        {
            hairStepsSorted.Add(hairSteps[i]);

            if (i > 0)
            {
                for (int j = hairStepsSorted.Count-2; j >= 0; j--)
                {
                    if(hairStepsSorted[j+1].transform.position.x < hairStepsSorted[j].transform.position.x)
                    {
                        HairStep dummy = hairStepsSorted[j];
                        hairStepsSorted[j] = hairStepsSorted[j + 1];
                        hairStepsSorted[j + 1] = dummy;
                    }
                }
            }
        }
    }

    public void WashSequentially()
    {
        if(index < hairStepsSorted.Count )
        {
            
            hairStepsSorted[index].WashPaticle();
            index += 1;
            m = (index * hairRoots.Length) / hairStepsSorted.Count;

            Invoke("EnableStraight", 0.2f); 
        }

        Invoke("WashSequentially", 0.02f);
    }

    public void WashHair(float per)
    {
        if (!washDone)
        {
            for (int i = 0; i < hairStepsSorted.Count; i++)
            {
                float p = (float)i / (float)hairStepsSorted.Count;
                if (p < (per + 0.2f) && p > (per - 0.2f))
                {
                    hairStepsSorted[i].WashPaticle();

                    //Debug.Log("p " + p);
                }



            }
            if(washedIndex >= hairStepsSorted.Count)
            {
                washDone = true;
                Invoke("EnableStraight",3f);
                
            }
        }

        

        //Debug.Log("washing " + per);
    }

    public void EnableStraight()
    {
        //for (int i = 0; i < hairRoots.Length; i++)
        //{
        //    if(i<= m)
        //    {
        //        if (!hairRoots[i].HairStraightMode)
        //        {
        //            hairRoots[i].StraightMode();
        //        }
        //        
        //    }
        //}
        if (!straighened)
        {
            for (int i = 0; i < hairRoots.Length; i++)
            {
                hairRoots[i].StraightMode();
            }

            straighened = true;
        }
        
    }

    public void EnableDrierMode()
    {
        if (straighened)
        {
            straighened = false;
        }
        
        for (int i = 0; i < hairRoots.Length; i++)
        {
            hairRoots[i].HairDrierModeFun();
        }
    }

    public void FoamTheHair()
    {
        SortHairSteps();
        for (int i = 0; i < hairSteps.Count; i++)
        {
            hairSteps[i].FoamParticle();
        }
    }

    public void HairDrierRunning(float f)
    {
        pointer =(int) Mathf.Lerp(0, (hairRoots.Length - 1), f);
        for (int i = 0; i < hairRoots.Length; i++)
        {
            hairRoots[i].HairDrierIntensity = 1 - (Mathf.Abs(i - pointer) * 0.1f);


        }
    }
    
}
