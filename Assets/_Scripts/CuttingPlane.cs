﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingPlane : MonoBehaviour
{
    public float hairLength;

    public HairRoot[] hairsToCut;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            for (int i = 0; i < hairsToCut.Length; i++)
            {
                CutHairEffect(hairsToCut[i] , this.gameObject);
            }
        }
    }

    public void CutHairEffect(HairRoot hair , GameObject goo )
    {
        //float dis = Mathf.Abs( hair.transform.InverseTransformPoint(this.gameObject.transform.position).y);

        float dis = Mathf.Abs(hair.transform.InverseTransformPoint(goo.transform.position).y);



        float cutFrac = dis / hairLength;
        //Debug.Log("dis " + dis + " frac " + cutFrac);
        //hair.HairMesh.GetComponent<Renderer>().material.SetColor("Color_759E13D3", Color.red);
        hair.HairMesh.GetComponentInChildren <Renderer>().material.SetFloat ("Vector1_37C7E26A", cutFrac);

        //hair.StraightMode();

        GameObject go = Instantiate(hair.HairMesh, hair.HairMesh.transform.position, hair.HairMesh.transform.rotation);
        go.GetComponentInChildren<Renderer>().material.SetFloat("Vector1_330397A5", 1);
        go.AddComponent<Rigidbody>();
        BoxCollider[] boxs = go.GetComponentsInChildren<BoxCollider>();
        foreach (var item in boxs)
        {
            item.enabled = false;
        }

        //go.GetComponent<Rigidbody>.()Add

        Destroy(go, 5f);
    }

    public void CUTTT( GameObject blade)
    {
        for (int i = 0; i < hairsToCut.Length; i++)
        {
            CutHairEffect(hairsToCut[i] , blade);
        }
    }
}
