﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManage : MonoBehaviour
{
    public static UiManage instance;

    private void Awake()
    {
        instance = this;
    }

    public GameObject[] PanelsStates;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToPanel(int state)
    {
        for (int i = 0; i < PanelsStates.Length; i++)
        {
            if(i == state)
            {
                PanelsStates[i].SetActive(true);
            }
            else
            {
                PanelsStates[i].SetActive(false);
            }
        }
    }
    
}
