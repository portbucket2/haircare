﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystem : MonoBehaviour
{
    public Vector3 touchPos;
    public Vector3 touchInPos;
    public Vector3 touchOutPos;

    public float progression;
    public float progressionOutput;

    public bool touched;

    public static InputSystem instance;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchInPos = Input.mousePosition;
        }
        
        if (Input.GetMouseButton(0))
        {
            touchPos = Input.mousePosition;

            progression = (touchPos.x - touchInPos.x)*5 / Screen.width;

            progression = Mathf.Clamp(progression, -1, 1);

            touched = true;
        }
        else
        {
            touched = false;
        }

        if (Input.GetMouseButtonUp(0))
        {
            touchOutPos = Input.mousePosition;
            progression = 0;
        }

        progressionOutput = Mathf.Lerp(progressionOutput, progression, Time.deltaTime * 10);
    }
}
