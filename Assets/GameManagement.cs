﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour
{
    public int levelIndex;
    public enum States
    {
        idle , walk , sit , shampoo , rinse , wash , dry , cut , done,
    }
    public States state;
    public static GameManagement instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToState(int i)
    {
        state = (States)i;
        UiManage.instance.GoToPanel(i);
    }
}
